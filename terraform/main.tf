data "azurerm_client_config" "current" {}

resource "azurerm_resource_group" "rg" {
  name = "${var.name}"
  location = "West Europe"
  tags = {
    name = "SIMON MANDEREAU"
  }
}

resource "azurerm_virtual_network" "network" {
  name                = "${var.name}-network"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
}

resource "azurerm_subnet" "subnet" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.network.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_public_ip" "publicIp" {
  name                = "acceptanceTestPublicIp12"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  allocation_method   = "Dynamic"
}

resource "azurerm_network_interface" "nic" {
  name                = "${var.name}-nic"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = azurerm_public_ip.publicIp.id
  }
}



resource "azurerm_linux_virtual_machine" "virtual" {
  name                = "${var.name}-machine"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  size                = "Standard_B1ls"
  admin_username      = "nico12345"
  admin_password      = "qLcmjdmLypDGfUkl9"
  network_interface_ids = [
    azurerm_network_interface.nic.id,
  ]

  tags = {
    name = "SIMON MANDEREAU"
  }


  admin_ssh_key {
    username   = "nico12345"
    public_key = file("./sshpub.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-focal"
    sku       = "20_04-lts"
    version   = "latest"
  }
}

