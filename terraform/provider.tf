provider "azurerm" {
  skip_provider_registration="true"
  features {}
}

terraform {
  backend "azurerm" {}
  required_providers {
    azurerm= {
      source="hashicorp/azurerm"
      version = "3.71.0"
    }
  }
}
